<?php

class Document extends \Eloquent {
	protected $fillable = ['title',
'entity_type',
'entity_ID',
'type',
'fullpath',
'filename',
'foldername',
'extension',
'filetype',
'thumnaildir',
'userID',
'deleted'];

public function landlord(){
	return $this->belongsTo('Landlord','entity_ID','id');
}
public function customer(){
	return $this->belongsTo('Customer','entity_ID','cust_id');
}
}