<?php

class Kin extends \Eloquent {
	protected $fillable = ['fname','mname','lname','contacts','customer_id'];
}