<?php

class Rent extends \Eloquent {
	protected $fillable = [
		'rent_id',
		'rent_houseID',
		'rent_tenantID',
		'rent_monthlyFee',
		'rent_type',
		'rent_advance',
		'rent_firstmonthpaid',
		'rent_nextpaydate',
		'deleted',
		'rent_lastpaydate',
		'rent_balance'
	];
	protected $primaryKey = 'rent_id';
	public function tenant(){
		return $this->belongsTo('Tenant','tent_rentID','rent_id');
	}
	public function payments(){
		return $this->hasMany('Rentpayment','paym_rentID','rent_id');
	}
}