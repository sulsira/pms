<?php

class Tenant extends \Eloquent {
	protected $fillable = [
'tent_id',
'tent_houseID',
'tent_compoundID',
'tent_rentID',
'tent_personid',
'tent_advance',
'tent_monthlyFee',
'tent_paymentStype',
'tent_status'
];

	public function house(){
		return $this->belongsTo('House','tent_houseID','hous_id');
	}
	public function person(){
		return $this->belongsTo('Person','tent_personid','id');
	}
	public function rents(){
		return $this->hasMany('Rent','rent_tenantID','tent_id');
	}
}