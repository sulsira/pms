<?php namespace services\validators;

class RentPayment extends Validate{
		public static $rules = [
		'payment_type'=> 'max:200',
		'payment_remark'=> 'max:200',
		'payment_house'=> 'max:200',
		'date_paid'=> 'max:200',
		'amount_paid'=> 'max:200',
		'number_months'=> 'max:200'
		// 'user_password'=> 'required|max:200|exists:users,password'
		// 'payment_code'=> 'required|max:200'
	];
	public function __construct($attributes = null){
		$this->attributes = $attributes ?: \Input::all();
	}
}