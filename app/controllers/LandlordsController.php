<?php

class LandlordsController extends \AdminController {

	/**
	 * Display a listing of the resource.
	 * GET /landlords
	 *
	 * @return Response
	 */
	public function index()
	{
		// documents
		// 
		$ll = Landlord::with('person.contacts','compounds')->whereRaw('ll_personid != ? AND deleted = ?',[0,0])->get();
		$ll = ($ll)? $ll->toArray() : [];
		// var_dump($ll[9]);
		// dd(head($ll));
		$this->layout->content = View::make('admin.Landlords.index')->with('landlords',$ll);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /landlords/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('admin.Landlords.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /landlords
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$person = array();
		$staff = array();
		$address = array();
		$contact = array();
		$ll = array();
		$done = false;

		if ($input) :
			foreach ($input as $k => $table) {
				if (is_array($table)) {
					if ($k == 'person') {
						$V = new services\validators\Person($table);
						if($V->passes()){
							$person = Person::create(array(
							'pers_fname' => $table['fname'], 
							'pers_mname' => ($table['mname']) ?: null, 
							'pers_lname' => $table['lname'], 
							'pers_type' => 'Landlord', 
							'pers_DOB' => $table['dob'], 
							'pers_gender' => $table['Pers_Gender'], 
							'pers_nationality' => $table['Pers_Nationality'], 
							'pers_ethnicity' => $table['Pers_Ethnicity'], 
							));
							if ($person->id) {
								$ll = Landlord::create(array('ll_fullname' =>$table['fname'].' '. $table['mname'] .' '. $table['lname'],'ll_personid'=>$person->id));	
								$done = true;
							}
						}else{
							$errors = $V->errors;
							return Redirect::back()->withErrors($errors)->withInput();							
						}


					}
					if ($k == 'address') {
						$address = $table;
						if ($person->id) {
							$address = array_add($address, 'Addr_EntityID', $person->id);
							$address = array_add($address, 'Addr_EntityType', 'Person');
							$V = new services\validators\Address($table);
							if($V->passes()){
								$address = Address::create($address);
							}
						}
						$errors = $V->errors;

					}
					if ($k == 'contact') {

						if ($person->id) {

							$V = new services\validators\Contact($table);
							foreach ($table as $key => $value) {
								if($V->passes()){

									if(!empty($value)){
										$contact = Contact::create(array(
										'Cont_EntityID' => $person->id,	
										'Cont_EntityType' => 'Person',	
										'Cont_Contact' => $value,	
										'Cont_ContactType' =>  $key	
										));

									$contact = $contact->toArray();
									}
								}
							}
							$errors = $V->errors;
						}
					}
				}		
			}


		if($done){

					if ($input['photo']) {

							$not = 'media'.DS.'Landlord'.DS.$person->pers_fname.'_'.$person->pers_mname.'_'.$person->pers_lname.'_'.$person->id;
							$foldername = DS.$not.DS;
							// $full_dir = $foldername.'img'.DS.'full';

							$path = base_path().$foldername ;
							$thumbnail_dir = $not.DS.'img'.DS.'thumbnail'.DS;
							$fileexten = Input::file('photo')->getClientOriginalExtension();
							$filename = Input::file('photo')->getClientOriginalName();
							// dd($path);
							$shortened = base_convert(rand(10000,99999), 10, 36);
							$rename = $person->pers_fname.'_'.$shortened.'_photo_.'.$filename;


							$filetyp = Input::file('photo')->getMimeType();

							$fileexten = Input::file('photo')->getClientOriginalExtension();
							$uploaded = false;

							if ((File::exists($path)) && ( File::exists( $path.DS.'img'.DS ) ) && ( File::exists( $path.DS.'img'.DS.'thumbnail') ) ) {

								Input::file('photo')->move($path.DS.'img'.DS, $rename);
									#notice #01
										// there was a problem when trying to resize the image. the fix is go in the project dir (html but ! include)

									// we resize (250 X 300)
									$img = Image::make($not.$rename);
									$img->resize(250 , 300);
									$img->save($thumbnail_dir.$rename);	// we move to thumnails

								// store in the database
								Document::create(array(
										'title' => 'profile picture' ,
										'entity_type' => 'Landlord',
										'entity_ID' => $ll->id,
										'type' => 'Photo',
										'fullpath' => $path.DS.'img'.DS.$rename,
										'filename' => $filename,
										'foldername' => $foldername,
										'extension' => $fileexten,
										'filetype' => $filetyp,
										'thumnaildir' => $thumbnail_dir.$rename
									));
								
								# code...
							}else{
								File::exists($path) ?: mkdir($path);
								File::exists($path.DS.'img'.DS) ?: mkdir($path.DS.'img'.DS);
								File::exists($path.DS.'img'.DS.'thumbnail') ?: mkdir($path.DS.'img'.DS.'thumbnail');
								Input::file('photo')->move($path.DS.'img'.DS , $rename);
									// we resize (250 X 300)
									// dd($not.DS.$rename);
									$img = Image::make($not.DS.'img'.DS.$rename);
									$img->resize(250 , 300);
									$img->save($thumbnail_dir.$rename);	// we move to thumnails

								// store in the database
								Document::create(array(
										'title' => 'profile picture' ,
										'entity_type' => 'Landlord',
										'entity_ID' => $ll->id,
										'type' => 'Photo',
										'fullpath' =>  $path.DS.'img'.DS.$rename,
										'filename' => $filename,
										'foldername' => $foldername,
										'extension' => $fileexten,
										'filetype' => $filetyp,
										'thumnaildir' => $thumbnail_dir.$rename
									));
							}
					} #end of photo


					if ($input['document']) {


							$not = 'media'.DS.'Landlord'.DS.$person->pers_fname.'_'.$person->pers_mname.'_'.$person->pers_lname.'_'.$person->id;
							$foldername = DS.$not.DS;
							// $full_dir = $foldername.'img'.DS.'full';
							
							$path = base_path().$foldername ;
							$fileexten = Input::file('document')->getClientOriginalExtension();
							$filename = Input::file('document')->getClientOriginalName();
							// dd($path);
							$shortened = base_convert(rand(10000,99999), 10, 36);
							$rename = $person->pers_fname.'_'.$shortened.'_document_.'.$filename;
							$filetyp = Input::file('document')->getMimeType();
							$fileexten = Input::file('document')->getClientOriginalExtension();
							$uploaded = false;

							if ( File::exists($path)  && ( File::exists( $path.DS.'document'.DS ) ) )  {

								Input::file('document')->move($path , $rename);

									#notice #01
										// there was a problem when trying to resize the image. the fix is go in the project dir (html but ! include)

									// we resize (250 X 300)
									// $img = Image::make($not.$rename);
									// $img->resize(250 , 300);
									// $img->save($thumbnail_dir.$rename);	// we move to thumnails

								// store in the database
								Document::create(array(
										'title' => $filename ,
										'entity_type' => 'Landlord',
										'entity_ID' => $ll->id,
										'type' => 'Document',
										'fullpath' => $path.$rename,
										'filename' => $filename,
										'foldername' => $foldername,
										'extension' => $fileexten,
										'filetype' => $filetyp
									));
								
								# code...
							}else{
								File::exists($path) ?: mkdir($path);
								mkdir($path.DS.'document'.DS);
								Input::file('document')->move($path.DS.'document'.DS , $rename);
									// we resize (250 X 300)
									// $img = Image::make($not.$rename);
									// $img->resize(250 , 300);
									// $img->save($thumbnail_dir.$rename);	// we move to thumnails

								// store in the database
								Document::create(array(
										'title' => $filename ,
										'entity_type' => 'Landlord',
										'entity_ID' => $ll->id,
										'type' => 'Document',
										'fullpath' => $path.$rename,
										'filename' => $filename,
										'foldername' => $foldername,
										'extension' => $fileexten,
										'filetype' => $filetyp
									));
							}
					} #end of document
				Flash::message("Successfully added a Landlord");
				return Redirect::back();
			}else{
				return Redirect::back()->withErrors($errors)->withInput();							
			}
		endif;
	}

	/**
	 * Display the specified resource.
	 * GET /landlords/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// $ll = Landlord::with()->whereRaw('ll_personid != ? AND deleted = ? AND id = ?',[0,0,$id])->first();
		$ll = Landlord::with('person.contacts','compounds','documents')->whereRaw('ll_personid != ? AND deleted = ? AND id = ?',[0,0,$id])->first();
		$ll = ($ll)? $ll->toArray() : [];
		$this->layout->content = View::make('admin.Landlords.show')->with('landlord',$ll);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /landlords/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /landlords/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /landlords/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}