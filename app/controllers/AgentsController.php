<?php

class AgentsController extends \AdminController {

	/**
	 * Display a listing of the resource.
	 * GET /agents
	 *
	 * @return Response
	 */
	public function index()
	{
		$agents = Agent::with('person.contacts')->get();	
		$agents = $agents ? $agents->toArray() : []; #secure
		$this->layout->content = View::make('admin.agents.index')->with('agents',$agents);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /agents/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('admin.agents.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /agents
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$person = array();
		$staff = array();
		$address = array();
		$contact = array();
		$done = false;

		if ($input) :
			foreach ($input as $k => $table) {
				if (is_array($table)) {
					if ($k == 'person') {
						$V = new services\validators\Person($table);
						if($V->passes()){
							$person = Person::create(array(
							'pers_fname' => $table['fname'], 
							'pers_mname' => ($table['mname']) ?: null, 
							'pers_lname' => $table['lname'], 
							'pers_type' => 'Agent', 
							'pers_DOB' => $table['dob'], 
							'pers_gender' => $table['Pers_Gender'], 
							'pers_nationality' => $table['Pers_Nationality'], 
							'pers_ethnicity' => $table['Pers_Ethnicity'], 
							));
							if ($person->id) {
								$done = true;
							}
						}else{
							$errors = $V->errors;
							return Redirect::back()->withErrors($errors)->withInput();							
						}


					}
					if ($k == 'address') {
						$address = $table;
						if ($person->id) {
							$address = array_add($address, 'Addr_EntityID', $person->id);
							$address = array_add($address, 'Addr_EntityType', 'Person');
							$V = new services\validators\Address($table);
							if($V->passes()){
								$address = Address::create($address);
							}
						}
						$errors = $V->errors;

					}
					if ($k == 'contact') {

						

						if ($person->id) {

							$V = new services\validators\Contact($table);
							foreach ($table as $key => $value) {
								if($V->passes()){

									if(!empty($value)){
										$contact = Contact::create(array(
										'Cont_EntityID' => $person->id,	
										'Cont_EntityType' => 'Person',	
										'Cont_Contact' => $value,	
										'Cont_ContactType' =>  $key	
										));

									$contact = $contact->toArray();
									}
								}
							}
							$errors = $V->errors;
						}
					}

				}
			}
			if($done){
					Agent::create(array(
						'agen_persID'=> $person->id,
						'agen_contID'=> $contact['id'],
						'agen_addrID'=> ($address->id) ?: 0
					));
				Flash::message("Successfully added a Student");
				return Redirect::back();
			}else{
				return Redirect::back()->withErrors($errors)->withInput();							
			}
		endif;


	}

	/**
	 * Display the specified resource.
	 * GET /agents/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

		$cus = Agent::with('person.contacts','person.addresses')->where('agen_id','=',$id)->first();

		$agent = $cus ? $cus->toArray() : []; #secure


		$this->layout->content = View::make('admin.agents.show')->with('agent', $agent);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /agents/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /agents/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /agents/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}