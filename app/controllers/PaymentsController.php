<?php

class PaymentsController extends \AdminController {

	/**
	 * Display a listing of the resource.
	 * GET /payments
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /payments/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('admin.payments.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /payments
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		// array_push($input ,[);
		// $input['user_password'] = Hash::make($input['user_password']);
		$pay  = new services\Validators\Payment;

		if($pay->passes()){
			// Record the payment in payment table
					$paid = Payment::create(array(
						'paym_userID' =>Session::get('user_id'),
						'paym_custID' => $input['paym_cusID'],
						'paym_paidAmount' => $input['paym_paidAmount'],
						'paym_currentBal' => $input['paym_currentBal'],
						'paym_transDate' => $input['paym_transDate'],
						'paym_plotID' => $input['paym_plotID']
						));
					// dd($paid->toArray());
					// $trans = Transaction::create(array(
					// 	'trans_custID' => ,
					// 	'trans_payID' => ,
					// 	'trans_currentBal' => ,
					// 	'trans_dueBal' => ,
					// 	'trans_totalBal' => ,
					// 	'trans_status' => ,
					// 	'trans_visible' => 
					// 	));

				Flash::overlay('The payment had been made');
				return Redirect::back();
			// replicate and change the customer transactions

			// display the balance of the cutomer


			// $user = User::create(array(
			// 	'email'=> $input['email'],
			// 	'hpwd'=>Hash::make($input['password']),
			// 	'deleted'=> 1)
			// );
			// if ($user) {
			// 	UserRole::create(array(
			// 		'user_id' => $user->id,
			// 		'fullname' => $input['username'],
			// 		'privileges' => $input['Previleges'],
			// 		'userGroup' => $input['accountType']
			// 	));
			// 	Flash::overlay('Your have added a user');
			// 	return Redirect::back();
			// }


		}else{
			$errors = $pay->errors;
			return Redirect::back()->withErrors($errors)->withInput();							
		}
	}

	/**
	 * Display the specified resource.
	 * GET /payments/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$this->layout->content = View::make('admin.payments.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /payments/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /payments/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /payments/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}