<?php

class PartnersController extends \AdminController {

	/**
	 * Display a listing of the resource.
	 * GET /partners
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('admin.partners.index');
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /partners/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /partners
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /partners/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$this->layout->content = View::make('admin.partners.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /partners/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /partners/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /partners/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}