<?php #page specific processing
$image = array();
$person =  array();
$contacts = array();
$documents =  array();
if (!empty($landlord)) {
    foreach ($landlord as $key => $value) {
       if ($key == 'person') {
          $person = $value;
       }
       if ($key == 'documents') {
           foreach ($value as $d => $doc) {
                if ($doc['type'] == 'Photo') {
                   $image = $doc;
                }else{
                    $documents[] = $doc;
                }
           }
       }
    }
}

 ?>
@include('templates/top-admin')
@section('content')
   <div class="scope">
        <div class="hedacont">
            <div class="navbar">
                <div class="navbar-inner" id="scopebar">
                    <div class="container">
                        <a class="btn btn-navbar" data-toggle="collapse" data-target="navbar-responsive-collapse">
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </a>
                        <a class="brand" href="">Land lord Name : {{ucwords( $landlord['ll_fullname'] )}}</a>
                        <div class="nav-collapse collapse navbar-responsive-collapse">
                          <ul class="nav">  
                            <li><a href="#basic">General</a> </li>
                            <li><a href="#basic">Compounds</a> </li>
                            <li><a href="#basic">Houses</a> </li>
                            <li><a href="#basic">Tenants</a> </li>
                            <li><a href="#transactions">Transactions</a></li>
                            <li><a href="#payments">Payments</a></li>
                            <li><a href="{{route('land-lords.edit',$landlord['id'])}}">Edit</a> </li>
                           </ul>
                        </div><!-- /.nav-collapse -->
                    </div>
                </div><!-- /navbar-inner -->
            </div> 
            <div class="c-header">
                <?php if (!empty($image)): ?>
                    <ul class="thumbnails" id="thmb">
                        <li class="span2">
                          <a href="#" class="thumbnail">
                           {{HTML::image($image['thumnaildir'])}}
                          </a>
                        </li>
                    </ul>                      
                <?php endif ?>

            </div>           
        </div>  
    </div>  <!-- end of scope -->

    <div class="content-details clearfix">
            <div class="cc clearfix" >
                <hr>
                <h3>Basic information</h3>
                <hr id="basic">
                <div class="span8 clearfix">
                            <div class="row">
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">General information</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Fullname:</td>
                                            <td>{{ucwords($landlord['ll_fullname'] )}}</td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Birth day:</td>
                                            <td>{{ucwords($person['pers_DOB'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Gender:</td>
                                            <td> {{ucwords($person['pers_gender'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Nationality:</td>
                                            <td> {{ucwords($person['pers_nationality'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Ethniticity:</td>
                                            <td> {{ucwords($person['pers_ethnicity'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                    </tbody>

                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Contact Information</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($contacts)): ?>
                                            <?php foreach ($contacts  as $key => $value): ?>
                                                <tr>
                                                    <td>{{ucwords($value['Cont_ContactType'])}}:</td>
                                                    <td>{{ucwords($value['Cont_Contact'])}}</td>
                                                    <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>                                                
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </tbody>
                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Address</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($addresses)): ?>
                                            <?php foreach ($addresses as $key => $value): ?>
                                                <?php if (!empty($value)): ?>
                                                    <tr>
                                                        <td>Street: </td>
                                                        <td>{{$value['Addr_AddressStreet']}}</td>
                                                        <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Town: </td>
                                                        <td>{{$value['Addr_Town']}}</td>
                                                        <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>District: </td>
                                                        <td>{{$value['Addr_District']}}</td>
                                                        <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                    </tr>                                                        
                                                <?php endif ?>
                                            <?php endforeach ?>
                                          
                                        <?php endif ?>
                                    </tbody>
                                </table>
                            </div>
                </div>
            </div> <!-- a .cc -->
  <div class="cc clearfix" id="transactions">
    <h3>Transactions Records</h3>
    <hr>
    <table class="table">
      <thead>
        <tr>
            <th>House Number</th>
            <th>Tenant</th>
            <th>first deposit</th>
            <th>recorded by</th>
            <th>due balance</th>
            <th>current balance</th>
            <th>updated</th>
            <th>created</th>
        </tr>
      </thead>
      <tbody>
        <?php if (!empty($customer['transactions'])): ?>
          <?php foreach ($customer['transactions'] as $key => $trans): ?>
            <tr>
                <td>data</td> 
                <td>data</td> 
                <td>data</td> 
                <td>data</td> 
                <td>data</td> 
                <td>data</td> 
                <td>data</td> 
                <td>data</td> 
            </tr>                                              
            <?php endforeach ?>  
        <?php else: ?>
        <tr rowspan="8"><td>no transactions calculated yet!</td></tr>
        <?php endif ?>
      </tbody>
    </table>
  </div>
   <div class="cc clearfix" id="payments">
        <h3>Payments</h3>
    <hr>
    <table class="table">
      <thead>
        <tr>
            <th>Tenant</th>
            <th>date</th>
            <th>Amount</th>
            <th>recorded by</th>
            <th>initial balance</th>
            <th>due balance</th>
            <th>current balance</th>
            <th>created</th>
        </tr>
      </thead>
      <tbody>
        <?php if (!empty($customer['plots'])): ?>
          <?php foreach ($customer['plots'] as $key => $plot): ?>
            <tr>
                
            </tr> 
                <?php foreach ($plot['payments'] as $key1 => $value1): ?>
                    <tr>
                        <td><a href="">{{ucwords($plot['plot_name'])}}</a></td>
                        <td>{{e($value1['paym_transDate'])}}</td>
                        <td>{{e($value1['paym_paidAmount'])}}</td>
                        <td>{{e($value1['user']['email'])}}</td>
                        <td>{{e($value1['paym_balance'])}}</td>
                        <td>--</td>
                        <td>--</td>
                        <td>{{e($value1['created_at'])}}</td>
                    </tr>                             
                <?php endforeach ?>                                               
            <?php endforeach ?>  
        <?php endif ?>
      </tbody>
    </table>
   </div>
</div>
@stop
@include('templates/bottom-admin')