@include('templates/top-admin')
@section('content')
	<div class="cc">
	<section>
		<?php if (!empty($data)): ?>
			<div class="stats">	
				<a href="#">fetch results</a>
			</div>	
			<hr>
			<ul class="search-results">
				<?php foreach ($data as $key => $value): ?>
				<?php if ($key == 'person' ): ?>
					<?php foreach ($value as $key1 => $value1): ?>
						<li class="result">
							<div class="search-detail">
							<strong>
								<a href="<?php echo 'search/person?pers='.$value1['pers_type'].'&key='.$value1['id']; ?>">
								<?php echo ucwords($value1['pers_fname'].' '.$value1['pers_mname'].' '.$value1['pers_lname']); ?>
								</a>
							</strong>
							<p>
								<span>Person Type : </span> <strong>{{$value1['pers_type']}}</strong> |
								<span>Nationality : </span> <strong>{{$value1['pers_nationality']}}</strong> |
								<span>Gender : </span> <strong>{{$value1['pers_gender']}}</strong> |
							</p>
							</div>
							<hr>
						</li>							
					<?php endforeach ?>
				<?php endif ?>
				<?php if ($key == 'plots' ): ?>
				<?php foreach ($value as $ind => $plot): ?>
				<?php foreach ($plot as $plt => $plo): ?>
					<li class="result">
						<div class="search-detail">
						<strong>
							<a href="{{route('plots.show',$plo['plot_id'])}}">{{e(ucwords($plo['plot_name']))}}</a>
						</strong>
						<p>
							<span>Result Type : </span> <strong>{{e($key)}}</strong> |
							<span>plot size : </span> <strong>{{e($plo['plot_size'])}}</strong> |
							<span>plot price : </span> <strong>{{e($plo['plot_price'])}}</strong> 
						</p>
						</div>
						<hr>
					</li>						
				<?php endforeach ?>
					
				<?php endforeach ?>
				
				<?php endif ?>
				<?php if ($key == 'contact' ): ?>
					<?php foreach ($value as $ind => $cont): ?>
					<?php $perstype = ($cont['person']['pers_type'] == 'Staff')? 'staffs' : 'students'; ?>
						<li class="result">
							<div class="search-detail">
							<strong>
								<a href="<?php echo 'search/person?pers='.$cont['person']['pers_type'].'&key='.$cont['person']['id']; ?>">
								<?php echo ucwords($cont['person']['pers_fname'].' '.$cont['person']['pers_mname'].' '.$cont['person']['pers_lname']); ?>
								</a>
							</strong>
							<p>
								<span>Contact Type : </span> <strong>{{$cont['Cont_ContactType']}}</strong> |
								<span>Contact Info : </span> <strong>{{$cont['Cont_Contact']}}</strong> 
							</p>
							<p>
								<span>Person Type : </span> <strong>{{$cont['person']['pers_type']}}</strong> |
								<span>Nationality : </span> <strong>{{$cont['person']['pers_nationality']}}</strong> |
								<span>Gender : </span> <strong>{{$cont['person']['pers_gender']}}</strong> |
							</p>
							</div>
							<hr>
						</li>						
					<?php endforeach ?>
					
				<?php endif ?>
				<?php if ($key == 'course' ): ?>
				<?php foreach ($value as $key => $course): ?>
					<li class="result">
						<div class="search-detail">
						<strong>
							<a href="{{route('departments.courses.show',[$course['depart_id'],$course['id']])}}">{{e(ucwords($course['name']))}}</a>
						</strong>
						<p>
							<span>Search Type : </span> <strong>Course</strong> |
						</p>
						</div>
						<hr>
					</li>					
				<?php endforeach ?>
					
				<?php endif ?>					
				<?php endforeach; ?>				
			</ul>
			<div class="stats">	
					<a href="#">fetch results</a>
			</div>	
			<?php else: ?>
				<h3>Sorry there were no results</h3>
		<?php endif ?>

	</section>
	</div>
@stop
@include('templates/bottom-admin')