@include('templates/top-admin')
@section('content')
@include('__partials/modal-add-estate')
	<div class="c-header cc">
		<h3>Estate <a href="#myModal" role="button" data-toggle="modal"><i class="fa fa-plus"></i></a></h3>
	</div>
	<div class="cc clearfix">
		<div class="messages">
			@include('flash::message')
			@include('__partials/errors')
		</div>
		<hr>
        <ul class="thumbnails">
        	<?php if (!empty($estate)): ?>
        		<?php foreach ($estate as $key => $value): ?>
		          <li class="span4">
		            <div class="thumbnail">
		              <div class="caption">
		                <h3>{{ucwords($value['name'])}}</h3>
		                <hr>
		                <p>
							{{ucwords($value['location'])}}
		                </p>
		                 <span>Number of Plots:</span> <strong>{{count($value['plots'])}}</strong>
		                <hr>
		                <p><a href="{{route('estates.plots.create',$value['est_id'])}}" class="btn btn-primary">Add plot</a> <a href="{{route('estates.plots.index',[$value['est_id']])}}" class="btn <?php echo (count($value['plots']))? '' : 'disabled'?>">view plots</a></p>
		              </div>
		            </div>
		          </li>        			
        		<?php endforeach ?>
        		<?php else: ?>
        		<h4>there are no Estate recorded yet!</h4>
        	<?php endif ?>
        </ul>
	</div>
@stop
@include('templates/bottom-admin')