@include('templates/top-admin')
@section('content')
	<div class="cc">
        <!-- <hr> -->

    	<div>
        <hr>
    		<div class="aside bio">
                <div class="ch">
                    <h4 id="bio"><?php echo (ucwords($plot['plot_name'])) ?: 'Plot'; ?></h4>
                </div>
                <hr>
                <div class="details">
                    <div class="row">
                    </div>
                </div>
    		</div>	<!--#bio-->	

    	</div> <!-- #index -->
    </div> <!-- a .cc -->
  <div class="cc" id="transactions">
    <strong>Price: </strong><h3> D <?php echo ($plot['plot_price']) ?: ' 0'; ?></h3>
    <hr>
    <table class="table">
      <thead>
        <tr>
            <th>Plot Number</th>
            <th>Plot Size</th>
            <th>Plot Location</th>
            <th>Plot Availability</th>
            <!-- <th>Plot Customer</th> -->
            <!-- <th>Actions</th> -->
            <th>update</th>
            <th>create</th>

        </tr>
      </thead>
      <tbody>
        <?php if (!empty($plot)): ?>
            <tr>
                <td>{{ucwords($plot['plot_number'])}}</td>
                <td>{{ucwords($plot['plot_size'])}}</td>
                <td>{{ucwords($plot['plot_location'])}}</td>
                <td>{{ucwords($plot['plot_availability'])}}</td>
                <td>{{ucwords($plot['updated_at'])}}</td>
                <td>{{ucwords($plot['created_at'])}}</td>
            </tr>                                               
        <?php endif ?>
      </tbody>
    </table>
  </div>
@stop
@include('templates/bottom-admin')