<!-- Modal -->

<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Add a new payment</h3>
  </div>
	  {{Form::open(array('route'=>'tenants.payments.store'))}}
		  <div class="modal-body">
					<div class="level details">

						<div class="first">
							<div>
								{{Form::label('payment_type','Type')}}
								<select name="payment_type" id="enit" class="input-xlarge">
									<option>rent</option>
								</select>
							</div>
							<div>
								{{Form::label('payment_remark','remarks')}}
								{{Form::text('payment_remark',null,['class'=>'input-xlarge','placeholder'=>'Enter payment remarks'])}}
							</div>
							<div>
								{{Form::label('payment_house','For house')}}
								<select name="payment_house" id="enit" class="input-xlarge">
									<?php if (!empty($tenant['house'])): ?>
										<option value="{{$tenant['house']['hous_id']}}">{{ucwords($tenant['house']['hous_number'])}}</option>		
									<?php endif ?>
								</select>
							</div>
							<div>
								{{Form::label('date_paid','Date paid')}}
								{{Form::date('date_paid',['class'=>'input-xlarge','placeholder'=>'Enter date of payment'])}}
							</div>
						</div>
					</div>
					<div class="level details">
						<span>History </span>
						<hr>
						<div class="first ">
							<div>
								{{Form::label('date_paid','Last payment date')}}
								<span class="input-xlarge uneditable-input">Some value here</span>
							</div>
							<div>
								{{Form::label('next_due_Date','Last payment due date')}}
								<span class="input-xlarge uneditable-input">Some value here</span>
							</div>
						</div>
						<div class="first">
							<div>
								{{Form::label('last_amount_paid','Last amount paid')}}
								<span class="input-xlarge uneditable-input">Some value here</span>
							</div>
							<div>
								{{Form::label('due_balance','Due balance')}}
								<span class="input-xlarge uneditable-input">Some value here</span>
							</div>
						</div>
					</div>
					<div class="level details">
						<span>Monetary </span>
						<hr>
						<div class="first ">
							<div>
								{{Form::label('amount_paid','Amount')}}
								{{Form::number('amount_paid',['class'=>'input-xlarge','placeholder'=>'Enter the amount paid','step'=>'any'])}}
							</div>
							<div>
								{{Form::label('number_months','For how many months')}}
								{{Form::number('number_months',['class'=>'input-xlarge','placeholder'=>'Enter number of months only when needed','step'=>'any'])}}
							</div>
						</div>
					</div>
		  </div>
			<div class="modal-footer">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
					<button class="btn btn-primary" name="createUser">Save changes</button>
			</div><!-- end of modal footer -->		
	 {{Form::close()}}
</div>